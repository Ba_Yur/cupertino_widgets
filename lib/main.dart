import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int pickedDay = DateTime.now().day;
  int pickedYear = DateTime.now().year;
  int pickedMonth = DateTime.now().month;

  bool isGreen = true;

  double boxHeight = 200;

  @override
  Widget build(BuildContext context) {
    DateTime pickedDateTime = DateTime.now();
    return Scaffold(
      appBar: AppBar(
        title: Text('Cupertino Widgets'),
      ),
      body: Center(
        child: Column(
          children: [
            CupertinoSlider(value: boxHeight, onChanged: (value){
              setState(() {
                boxHeight = value;
              });
            },min: 0,max: 300,),
            CupertinoSwitch(value: isGreen , onChanged: (newValue) {
              setState(() {
                isGreen = newValue;
              });
            }),
            SizedBox(
              width: 300,
              child: Column(
                children: [
                  Text(
                    'pickedDay: ${pickedDay}',
                    style: TextStyle(fontSize: 20),
                  ),
                  Text(
                    'pickedMonth: ${pickedMonth}',
                    style: TextStyle(fontSize: 20),
                  ),
                  Text(
                    'pickedYear: ${pickedYear}',
                    style: TextStyle(fontSize: 20),
                  ),
                ],
              ),
            ),
            Container(
              height: boxHeight,
              child: CupertinoDatePicker(
                backgroundColor: isGreen ? Colors.greenAccent : Colors.grey,
                initialDateTime: DateTime.now(),
                minimumYear: 2010,
                maximumYear: 2050,
                mode: CupertinoDatePickerMode.date,
                onDateTimeChanged: (value) {
                  setState(() {
                    pickedDay = value.day;
                    pickedMonth = value.month;
                    pickedYear = value.year;
                  });
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
